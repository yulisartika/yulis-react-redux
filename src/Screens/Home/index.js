import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import Layout from "Components/Layout";

class Home extends Component {
  constructor() {
    super();
    this.state = {
      pokemons: [],
      isLoading: true,
      limit: 10,
      offset: 2,
      favorites: [],
    };
  }

  componentDidMount() {
    this.fetchdataDenganFetch();
  }

  fetchdataDenganFetch = () => {
    const { limit, offset } = this.state;
    this.setState({ isLoading: true });
    fetch(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`)
      .then((response) => response.json())
      .then((data) =>
        this.setState({ pokemons: data.results, isLoading: false })
      )
      .catch((error) => console.log(error));
  };

  render() {
    console.log(this.props); // untuk ngecek apakah data dari store itu udah masuk apa blm
    const { pokemons, isLoading, offset, favorites } = this.state;

    return (
      <Layout>
        <div className="home">
          <div className="home__title">Fave Pokees</div>
          <div className="home__grid container">
            {this.props.favPokemon.length === 0 || isLoading
              ? null
              : this.props.favPokemon.map((pokemon, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <Link
                        className="home__grid__item__content"
                        to={`/pokemon/${pokemon.id}`}
                      >
                        <span>{pokemon?.name}</span>
                      </Link>
                    </div>
                  );
                })}
          </div>

          <div className="home__title">Pokee Lists</div>
          <div className="home__grid container">
            {pokemons.length === 0 || isLoading
              ? null
              : pokemons.map((pokemon, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <button
                        className="home__grid__item__save"
                        onClick={
                          () =>
                            this.props.handleAddFavPokemon({
                              newValue: {
                                id: offset + index + 1,
                                name: pokemon.name,
                              },
                            })

                          // this.props.handleAddFavPokemon({
                          //   newValue: { id: index, name: pokemon.name },
                          // })
                        }
                      >
                        +
                      </button>

                      <Link
                        className="home__grid__item__content"
                        to={`/pokemon/${
                          offset === 0 ? index + 1 : offset + index + 1
                        }`}
                      >
                        <span>{pokemon.name}</span>
                      </Link>
                    </div>
                  );
                })}
          </div>
        </div>
      </Layout>
    );
  }
}

// mapStateToProps adalah fungsi untuk mapping data dari store-redux ke komponen yg menggunakan (terbaca sbg props)
const mapStateToProps = (state) => {
  return {
    favPokemon: state.favoritePokemon, // the value of state.favoritePokemon is an array and this thing is an empty array at the beginning
    // favBerries: state.favoriteBerries,
  };
};

// mapping dispatch as props
const mapDispatchToProps = (dispatch) => {
  return {
    handleAddFavPokemon: (params) => {
      dispatch({ type: "SET_FAVORITE_POKEMON", newValue: params.newValue }); // params itu argumen yg dikasih diatas nya object, jadiii, newValue kita set ke params.newValue (newValue itu key nya di ataas yg punya params)
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

// is handleAddFavPokemon a method? learn about object later
